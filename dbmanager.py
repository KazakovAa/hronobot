import sqlite3
import itertools


def createTable(conn, cursor):
    try:
        cursor.execute("DROP TABLE timetable;")
        cursor.execute("""CREATE TABLE timetable
                        (id_chat integer, in_date text, out_date text, day integer, month integer)
                    """)
        conn.commit()
    except sqlite3.OperationalError:
        pass

def createTableTime(conn, cursor):
    try:
        cursor.execute("DROP TABLE inptimes;")
        cursor.execute("""CREATE TABLE inptimes
                        (id_chat integer, time text)
                    """)
        conn.commit()
    except sqlite3.OperationalError:
        cursor.execute("""CREATE TABLE inptimes
                        (id_chat integer, time text)
                    """)
        conn.commit()

def saveEnteredTime(id_chat, time, conn, cursor):
    try:
        cursor.execute(f'DELETE FROM inptimes WHERE id_chat={id_chat}')
    except:
        pass

    try:
        cursor.execute(f'INSERT INTO inptimes VALUES ({id_chat}, "{time}")')
    except:
        pass
    conn.commit()

def deleteEnterTime(id_chat, conn, cursor):
    try:
        cursor.execute(f'DELETE FROM inptimes WHERE id_chat={id_chat}')
    except:
        print("В таблице отсутствует строки с данным ид")

def loadEnteredTime(id_chat, conn, cursor):
    cursor.execute(f'SELECT * FROM inptimes where id_chat={id_chat}')
    rows = cursor.fetchall()
    # print(rows)
    return rows[0][1] if rows[0] else rows[0]

def clearInptable(id_chat, conn, cursor):
    try:
        cursor.execute(f'DELETE FROM inptimes WHERE id_chat={id_chat}')
    except:
        pass

def pushData(id_chat, enteredTime, leavedTime, date, conn, cursor):
    cursor.execute(f'INSERT INTO timetable VALUES ({id_chat}, "{enteredTime}", "{leavedTime}", {date["day"]}, {date["month"]} )')
    conn.commit()

def getAllData(id_chat, month, conn, cursor):
    cursor.execute(f'SELECT * FROM timetable where id_chat="{id_chat}" AND month={month}')
    rows = cursor.fetchall()
    return rows

def getAllTable(conn, cursor):
    data = cursor.execute('SELECT * FROM timetable;')
    result = {}
    for id_, iter_ in itertools.groupby(data, key=lambda r: r[0]):
        f = list(iter_)
        try: 
            result[id_]
        except KeyError:
            result[id_] = []

        result[id_].append(f[0])
    return result

    # with open('input.txt', 'w') as f:
    #     for row in rows:
    #         f.write(str(row))

def getLastMonth(conn, cursor):
    cursor.execute('SELECT month FROM timetable;')
    return (cursor.fetchall())[-1][0]