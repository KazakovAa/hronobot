token = "948789565:AAEK85fC4kjAD-BPTnh0eivMmv5t90qXlDI"

import telebot
from telebot import types

from dbmanager import *
from excelmanager import *


def initBot(token_):
    return telebot.TeleBot(token_)

bot = initBot(token)
enteredTime = None
leavedTime = None
sum_ = ""


def globalSum(diff, sum_):
    if sum_ == "-":
        return diff
    else:
        [hDiff, mDiff, _] = diff.split(":")
        [h, m, _] = sum_.split(":")
        # высчитываем минуты и сдвиг по часам
        tmp_sum = int(m) + int(mDiff)
        offsetHour = tmp_sum // 60
        newM = tmp_sum  % 60
        newM = f"0{newM}" if newM < 10 else newM
        # высчитываем часы
        tmp_sum = int(h) + int(hDiff) + offsetHour
        newH = tmp_sum

        return f"{newH}:{newM}:00"


def timeToMsk(tm):
    time = (str(tm)).split(".")[0]
    offset = 7
    hour = int(time.split(":")[0])
    resultTime = f'{hour+offset}:{time.split(":")[1]}:{time.split(":")[2]}'
    return datetime.datetime.strptime(resultTime, "%H:%M:%S").time()


def calcLeavedTime(entTime):
    offset = 9
    [h, m, s] = str(entTime).split(":")
    newH = (int(h) + offset) % 24

    newM = int(m) + 20
    if newM >= 60:
        newM = newM % 60
        newH += 1
    
    return datetime.datetime.strptime(f"{newH}:{newM}:{s}", "%H:%M:%S").time()

def calcSub(et,lt,time):
    eh = int(str(et).split(":")[0])
    lh = int(str(lt).split(":")[0])
    [h, m, s] = time.split(":")
    print(h)
    print("##############")
    if( not (int(lh) <= 12 and int(h) <= 4) ):
        newM = int(m) - 20
        newH = int(h) - 1
        if newM < 0:
            newM = newM % 60
            newH -= 1

        if newM < 30:
            newM = 0
        else:
            newM = 30
        return datetime.datetime.strptime(f"{newH}:{newM}:00", "%H:%M:%S").time()
    else:
        return datetime.datetime.strptime(f"{h}:{m}:00", "%H:%M:%S").time()

def isTime(time):
    try:
        return datetime.datetime.strptime(time, "%H:%M:%S").time()
    except:
        return None

def dateNow():
    date = str(datetime.datetime.now()).split(" ")[0]
    [_, month, day] = date.split("-")
    return {"day": int(day), "month": int(month)}

def sendFile(msg):
    bf = open('./excel_report/month.xlsx', 'rb')
    print(type(bf))
    bot.send_document(msg.chat.id, bf)
    bf.close()


# =========== служебные комманды =============

@bot.message_handler(commands=['i'])
def handle_my_id(msg):
    bot.send_message(msg.chat.id, msg.chat.id)

@bot.message_handler(commands=['report'])
def handle_get_report(msg):
    fill_in_the_table(calcSub)
    with open('./excel_report/month.xlsx', 'rb') as f:
        bot.send_document(msg.chat.id, f)

@bot.message_handler(commands=['restartdb'])
def handle_restartdb(msg):
    with sqlite3.connect("store.db") as conn:
        cursor = conn.cursor()
        createTableTime(conn, cursor)

@bot.message_handler(commands=['check'])
def handle_check(msg):
    bot.send_message(msg.chat.id, f'enteredTime: {str(enteredTime)}')

@bot.message_handler(commands=['lastmonth'])
def handle_lastmonth(msg):
    with sqlite3.connect("store.db") as conn:
        cursor = conn.cursor()
        bot.send_message(msg.chat.id, getLastMonth(conn, cursor))

# ============================================

@bot.message_handler(commands=['help'])
def handle_help(msg):
    helpMsg = """
[ЗАШЕЛ] -- фиксирует время, когда зашел на проходную

[ВЫШЕЛ] -- фиксируется время, когда вышел с проходной
после этого проявится строка в отчете

[отчет] -- показывает информацию за текущий месяц по часам

[КОГДА ВЫЙТИ] - высчитывает время выхода с проходной с учетом 20 минут

[/start] -- для обновления бота

[set xx:vv:cc in|out] -- 
"""
    bot.send_message(msg.chat.id, helpMsg) #f'enteredTime: {str(enteredTime)}')

@bot.message_handler(commands=['start'])
def handle_start(msg):
    welcome = """
         ╔═════════╗
         ║░╔═╦═╦═╗░║
         ║░║ ║ ║ ║░║
         ║░║     ║░║
         ║░╠═════╣░║
         ║░║  ═══╣░║
         ║░║  ═══╣░║
         ║░╠══╦══╣░║
         ║░║  ║  ║░║
         ║░║  ╚══╣░║
         ║░╠═════╣░║
         ║░║  ╔══╣░║
         ║░║  ╚══╣░║
         ║░╠═════╣░║
         ║░║  ║  ║░║
         ║░║  ║  ║░║
         ║░╠══╦══╣░║
         ║░║     ║░║
         ║░║  ║  ║░║
         ║░╠═╩═╩═╣░║
         ║░║  ═══╣░║
         ║░║  ═══╣░║
         ║░╚═════╝░║
         ╚═════════╝
    """
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
    markup.add('ЗАШЕЛ', 'ВЫШЕЛ', 'отчет', "КОГДА ВЫЙТИ")
    bot.send_photo(msg.chat.id, open('Cheese_Wed-02Oct19_11.41.png', 'rb'));
    outmsg = bot.reply_to(msg, "@TO HRONOBOT@", reply_markup=markup)
    # bot.send_message(msg.chat.id, welcome)
    # bot.register_next_step_handler(msg, markup = types.ReplyKeyboar process_step)

""" JXegZpWNPJ201SDhT56Clm8KACw2UteYuXseEzptGIToG7RKqbGDkHAjELoXf-KSwRIvlgtZo18lZg6AuYeeHkxRlArJfLb-xA9axBh9A-Yjl8LC1J-nGwG35gXeGyKMCDb-fN4TS4ncQ-TsfLQ7RZB1YbjNMc-I9qQgPUAYAxg- """


def texthandler_enter(msg):
    enteredTime = timeToMsk(datetime.datetime.now().time())
    with sqlite3.connect("store.db") as conn:
        cursor = conn.cursor()
        saveEnteredTime(msg.chat.id, str(enteredTime), conn, cursor)
    bot.send_message(msg.chat.id, f'Ты зашел на проходную в:\n {str(enteredTime)}')

def texthandler_leave(msg): 
    with sqlite3.connect("store.db") as conn:
        cursor = conn.cursor()
        enteredTime = loadEnteredTime(msg.chat.id, conn, cursor)
        deleteEnterTime(msg.chat.id, conn, cursor)

    leavedTime = timeToMsk(datetime.datetime.now().time())
    if (leavedTime != None and enteredTime != None):
        diff = datetime.datetime.strptime(f"21/11/06 {leavedTime}", "%d/%m/%y %H:%M:%S") - datetime.datetime.strptime(f"21/11/06 {enteredTime}", "%d/%m/%y %H:%M:%S")
        bot.send_message(msg.chat.id, f'Ты вышел с проходной в:\n {str(leavedTime)}')
        bot.send_message(msg.chat.id, f'Отработал:\n {str(calcSub(enteredTime, leavedTime, str(diff)))}')
        with sqlite3.connect("store.db") as conn:
            cursor = conn.cursor()
            lastmonth = getLastMonth(conn, cursor)
            curDate = dateNow()
            if(int(curDate["month"])  > int(lastmonth) or (int(lastmonth) - int(curDate["month"])) == 11): # int(curDate["month"]) 
                fill_in_the_table(calcSub)
                sendFile(msg)

            pushData(msg.chat.id, enteredTime, leavedTime, dateNow(), conn, cursor)
        enteredTime = None
        leavedTime = None
    else:
        bot.send_message(msg.chat.id, f'НЕЛЬЗЯ ВЫЙТИ С ПРОХОДНОЙ НЕ ВХОДЯ ТУДА')

def texthandler_report(msg):
    allInfo = None
    sum_ = "-"
    with sqlite3.connect("store.db") as conn:
        cursor = conn.cursor()
        date = dateNow()
        allInfo = getAllData(msg.chat.id, date["month"], conn, cursor)

    report = []
    currsum = None
    for row in allInfo:
        #print(sum_)
        (id_, enteredTime_ , leavedTime_, day_, month_) = row
        diff = datetime.datetime.strptime(f"21/11/06 {leavedTime_}", "%d/%m/%y %H:%M:%S") - datetime.datetime.strptime(f"21/11/06 {enteredTime_}", "%d/%m/%y %H:%M:%S")
        report.append( [enteredTime_, leavedTime_, str(calcSub(enteredTime_, leavedTime_, str(diff))), str(day_)] )
        sum_ = globalSum(str(calcSub(enteredTime_, leavedTime_, str(diff))), sum_)

    tmp = map(lambda x: "  | ".join(x), report)
    result = "\n".join(tmp)
    result = " зашел   вышел   отработано   день\n" + "-"*60+ "\n" + result + "\nВсего часов: " + sum_
    sum_ = "-"
    bot.send_message(msg.chat.id, result)
    return sum_

def texthandle_calcLeaveTime(msg):
    enttime = None
    with sqlite3.connect("store.db") as conn:
        cursor = conn.cursor()
        enttime = loadEnteredTime(msg.chat.id, conn, cursor)
    bot.send_message(msg.chat.id, f"Нужно выйти в:\n{str(calcLeavedTime(enttime))}")


def texthandler_set(msg):
    emojiGood = b"\xE2\x9C\x94".decode()
    emojiBad = b"\xE2\x9C\x96".decode()
    [command, date, param] = (msg.text).split(" ")
    time = isTime(date)
    if time:
        if param == "in":
            enteredTime = time
            with sqlite3.connect("store.db") as conn:
                cursor = conn.cursor()
                saveEnteredTime(msg.chat.id, str(enteredTime), conn, cursor)

            bot.send_message(msg.chat.id, f"Установлено время входа {emojiGood}")

        elif param == "out":
            leavedTime = time
            bot.send_message(msg.chat.id, f"Установлено время выхода {emojiGood}")
            with sqlite3.connect("store.db") as conn:
                cursor = conn.cursor()
                enteredTime = loadEnteredTime(msg.chat.id, conn, cursor)
            if (leavedTime != None and enteredTime != None):
                diff = datetime.datetime.strptime(f"21/11/06 {leavedTime}", "%d/%m/%y %H:%M:%S") - datetime.datetime.strptime(f"21/11/06 {enteredTime}", "%d/%m/%y %H:%M:%S")
                bot.send_message(msg.chat.id, f'Ты вышел с проходной в:\n {str(leavedTime)}')
                bot.send_message(msg.chat.id, f'Отработал:\n {str(calcSub(enteredTime, leavedTime, str(diff)))}')
                with sqlite3.connect("store.db") as conn:
                    cursor = conn.cursor()
                    pushData(msg.chat.id, enteredTime, leavedTime, dateNow(), conn, cursor)
                enteredTime = None
                leavedTime = None
    else:
        bot.send_message(msg.chat.id, f"Не удалось {emojiBad}")

@bot.message_handler(content_types=['text'])
def handle_action(msg):
    global leavedTime, enteredTime
    if msg.text == "ЗАШЕЛ":
        texthandler_enter(msg)

    elif msg.text == "ВЫШЕЛ":
        texthandler_leave(msg)

    elif msg.text == "отчет":
        texthandler_report(msg)

    elif "set" in msg.text:
        texthandler_set(msg)

    elif msg.text == "КОГДА ВЫЙТИ":
        texthandle_calcLeaveTime(msg)
        

bot.polling()