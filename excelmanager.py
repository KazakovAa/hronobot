# from openpyxl import sheet, Workbook
import openpyxl
import os
import datetime
import sqlite3
from dbmanager import getAllTable

def init_wb():
    try:
        os.remove("./excel_report/month.xlsx")
    except OSError:
        pass

    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = "main sheet"
    return wb

# wb = init_wb()

def write_header(id_row, wb):
    sheet = wb.active
    header = ("Имя", "Время входа", "Время выхода", "День", "Часов отработано")
    
    for id_column, el in enumerate(header):
        cell = sheet.cell(row = id_row, column = id_column + 1)
        cell.value = el

    return wb

def write_row_in_excel(row, id_row, wb):
    sheet = wb.active
    
    for id_column, el in enumerate(row):
        cell = sheet.cell(row = id_row, column = id_column + 1)
        cell.value = el
    return wb

def fill_in_the_table(calcFun):
    wb = init_wb()

    names = {
        285601011: "Казаков Антон",
        974247845: 'Волкова Ирина',
        978690814: 'Хурчак Александра'
    }
    db = None    
    with sqlite3.connect("store.db") as conn:
        cursor = conn.cursor()
        db = getAllTable(conn,cursor)

    count = 1
    wb = write_header(count, wb)
    count += 1
    
    for id_ in db:
        name = names[id_]
        data = db[id_]

        for row in data:
            count += 1
            row_ = row[1:-1]
            enteredTime, leavedTime = row_[0], row_[1]
            diff = datetime.datetime.strptime(f"21/11/06 {leavedTime}", "%d/%m/%y %H:%M:%S") - datetime.datetime.strptime(f"21/11/06 {enteredTime}", "%d/%m/%y %H:%M:%S")
            sub = str(calcFun(enteredTime, leavedTime, str(diff)))
            wb = write_row_in_excel([name] + list(row_) + [sub], count, wb)

    wb.save(filename = './excel_report/month.xlsx')


