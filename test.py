

sum_ = ""
def globalSum(diff):
    global sum_

    if sum_ == "":
        sum_ = diff
    else:
        [hDiff, mDiff, _] = diff.split(":")
        [h, m, _] = sum_.split(":")
        # высчитываем минуты и сдвиг по часам
        tmp_sum = int(m) + int(mDiff)
        offsetHour = tmp_sum // 60
        newM = tmp_sum  % 60
        newM = f"0{newM}" if newM < 10 else newM
        # высчитываем часы
        tmp_sum = int(h) + int(hDiff) + offsetHour
        newH = tmp_sum

        sum_ = f"{newH}:{newM}:00"

globalSum("09:30:00")
globalSum("08:00:00")
globalSum("09:30:00")
globalSum("06:30:00")
print(sum_)